# Import modules
import tensorflow as tf
import numpy as np
from tensorflow.keras import backend as K
from tensorflow.keras.layers import Input, Flatten, Reshape, Dense, Activation
from tensorflow.keras.layers import Conv1D, GlobalMaxPooling1D, Dropout, Lambda
from tensorflow.keras.models import Model
from spektral.layers import GCNConv, GraphSageConv, TAGConv, AGNNConv, GATConv, GINConv, ChebConv
from spektral.layers import GeneralConv, APPNPConv, GINConv, EdgeConv, TAGConv, APPNPConv, GCSConv
#from spektral.data import Graph
#from spektral.transforms import LayerPreprocess

import sys
sys.path.append("/media/raphael/SSD2/EvoAnnotDL/scriptPython/functions") 
from layers import *


vocab_size=4 # There are 4 letters in DNA (A, C, G and T). N letter is treated as 0 in one-hot encoding.


batch_size=100 # TO REMOVE


##
# Build baseline CNN or the CNN-GNN model for semi-supervized learning
def build_model(model_type, # model="CNN-GNN" or model="CNN"
              sequence_length, # Length of the DNA sequence to process
              num_genomes, # Number of genomes
              num_filters=64, # Number of convolutional kernels
              kernel_size=24, # Kernel size of the first convolutional layer
              dilated_conv_kernel_size=3, # Kernel size of the dilated convolutional layers (only if cnn_type="deep")
              cnn_type="shallow", # type="shallow" (one convolutional layer) or "deep" (many convolutional layers)
              activation="sigmoid", # activation="sigmoid" (classification) or activation="softplus" (regression)
              conv_activation="relu", # Convolution activation (any activation used in Keras)
              conv_type="simple", # Convolution type "simple", "bottleneck" , "inverted", "inverted_linear", "separable"
              num_dilated_convlayers=10, # Number of dilated residual convolutional layers if type="deep"
              graph_conv="GraphSAGE"): # graph_conv="GraphSAGE", "GCNConv", "GCSConv", "APPNPConv", "GATConv"
    
    if model_type=="CNN":
	    model=build_CNN(sequence_length=sequence_length,num_filters=num_filters,
	    		kernel_size=kernel_size,dilated_conv_kernel_size=dilated_conv_kernel_size,
	    		cnn_type=cnn_type,
		       activation=activation,conv_activation=conv_activation,
		       conv_type=conv_type,
		       num_dilated_convlayers=num_dilated_convlayers)
    elif model_type=="CNN-GNN":           
	    model=build_CNN_GNN(sequence_length=sequence_length,num_genomes=num_genomes,num_filters=num_filters,
		            kernel_size=kernel_size,dilated_conv_kernel_size=dilated_conv_kernel_size,
		            cnn_type=cnn_type,activation=activation,
		            conv_activation=conv_activation,conv_type=conv_type,
		            num_dilated_convlayers=num_dilated_convlayers,
		            graph_conv=graph_conv)
    return model


# Baseline convolutional neural network
def build_CNN(sequence_length, # Length of the DNA sequence to process
              num_filters=64, # Number of convolutional kernels
              kernel_size=24, # Kernel size of the first convolutional layer
              dilated_conv_kernel_size=3, # Kernel size of the dilated convolutional layers (only if cnn_type="deep")
              cnn_type="shallow", # type="shallow" (one convolutional layer) or "deep" (many convolutional layers)
              activation="sigmoid", # activation="sigmoid" (classification) or activation="softplus" (regression)
              conv_activation="relu", # Convolution activation (any activation used in Keras)
              conv_type="simple", # Convolution type "simple", "bottleneck" , "inverted", "separable"
              num_dilated_convlayers=10 # Number of dilated residual convolutional layers if type="deep"
              ): 
    X_in=Input(shape=(sequence_length,))

    CNN=OneHot(input_dim=vocab_size, input_length=sequence_length)(X_in)
    CNN=Conv1D(num_filters, kernel_size, activation=conv_activation, 
               input_shape = [sequence_length,vocab_size],padding="same")(CNN)
    if cnn_type=="deep":
        CNN=DilatedConv1D(filters=num_filters,kernel_size=dilated_conv_kernel_size,
        		num_dilated_convlayers=num_dilated_convlayers,
        		conv_type=conv_type, batch_norm=False, activation='relu',dropout=0)(CNN)    
    CNN=GlobalMaxPooling1D()(CNN)
    CNN=Dropout(0.2)(CNN)
    CNN=Dense(10, activation='linear')(CNN)
    CNN=Dropout(0.2)(CNN)
    CNN=Dense(1, activation=activation)(CNN)
    
    y_out=CNN

    model=Model(inputs=X_in, outputs=y_out, name="CNN")
    return model
    
    
# CNN-GNN model for semi-supervized learning
def build_CNN_GNN(sequence_length, # Length of the DNA sequence to process
              num_genomes, # Number of genomes
              num_filters=64, # Number of convolutional kernels
              kernel_size=24, # Kernel size of the first convolutional layer
              dilated_conv_kernel_size=3, # Kernel size of the dilated convolutional layers (only if cnn_type="deep")
              cnn_type="shallow", # type="shallow" (one convolutional layer) or "deep" (many convolutional layers)
              activation="sigmoid", # activation="sigmoid" (classification) or activation="softplus" (regression)
              conv_activation="relu", # Convolution activation (any activation used in Keras)
              conv_type="simple", # Convolution type "simple", "bottleneck" , "inverted", "separable"
              num_dilated_convlayers=10, # Number of dilated residual convolutional layers if type="deep"
              graph_conv="GraphSAGE"): # graph_conv="GraphSAGE", "GCNConv", "GCSConv", "APPNPConv", "GATConv"
    X_in=Input(shape=(None,sequence_length,))
    A_in=Input(shape=(num_genomes,num_genomes,),sparse=False,name="A_in")
    
    CNN=OneHot(input_dim=vocab_size, input_length=sequence_length)(X_in)
    CNN=Conv1D(num_filters, kernel_size, activation='relu', 
               input_shape = [sequence_length,vocab_size],padding="same")(CNN)
    if cnn_type=="deep":
        CNN=DilatedConv1D(filters=num_filters,kernel_size=3, num_dilated_convlayers=num_dilated_convlayers,
        		conv_type=conv_type, batch_norm=False, activation='relu',dropout=0)(CNN)    
    CNN=GlobalMaxPooling1DGNN()(CNN)
    CNN=Dropout(0.2)(CNN)
    CNN=Dense(10, activation='linear')(CNN)
    CNN=Dropout(0.2)(CNN)
    
    if graph_conv=="GraphSAGE":
        GNN=GraphSage(units=1,activation="linear")([CNN,A_in]) # 
    elif graph_conv=="GCNConv":
        GNN=PreprocessGraph(conv_layer="GCNConv", batch_size=batch_size)(A_in)
        GNN=GCNConv(channels=1,activation="linear")([CNN,GNN]) 
    elif graph_conv=="GCSConv":
        GNN=PreprocessGraph(conv_layer="GCSConv", batch_size=batch_size)(A_in)
        GNN=GCSConv(channels=1,activation="linear")([CNN,GNN]) 
    elif graph_conv=="APPNPConv":
        GNN=PreprocessGraph(conv_layer="APPNPConv", batch_size=batch_size)(A_in)
        GNN=APPNPConv(channels=1,activation="linear")([CNN,GNN]) 
    elif graph_conv=="GATConv":
        GNN=PreprocessGraph(conv_layer="GATConv", batch_size=batch_size)(A_in)
        GNN=GATConv(channels=1,activation="linear")([CNN,GNN]) 
       
    GNN=Activation(activation=activation)(GNN)
    y_out=Lambda(reshape_y_out_GNN,name="reshape_y_out_GNN")(GNN)

    model=Model(inputs=[X_in,A_in], outputs=y_out, name="CNN-GNN")
    return model
    

