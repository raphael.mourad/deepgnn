import pandas as pd
import numpy as np
import scipy.sparse as sp
import gzip
import pickle
import h5py
import lzma
import sys
import gc
import multiprocessing
#from kmer import *
from os import path
from Bio import SeqIO
sys.path.append("/media/raphael/SSD2/EvoAnnotDL/scriptPython/functions") 


def readRegularFastaFile(fasta_file):
	seql_list=[]
	with gzip.open(fasta_file, "rt") as handle:
		for record in SeqIO.parse(handle, "fasta"):
			seqj=str(record.seq)
			seqj=seqj.replace("A", "0")
			seqj=seqj.replace("C", "1")
			seqj=seqj.replace("G", "2")
			seqj=seqj.replace("T", "3")
			seqj=seqj.replace("N", "4")
			mapnumj=map(int, list(seqj))
			listnumj=list(mapnumj)
			seql_list.append(listnumj)			
				
	X_array=np.array(seql_list).astype(np.int8)
	return X_array
	
	
# Read fasta file and convert to numeric encoding of sequences
def readFastaFile(fasta_file):
    seql_list=[]
    yc_list=[]
    yr_list=[]
    o_list=[]
    with gzip.open(fasta_file, "rt") as handle:
        for record in SeqIO.parse(handle, "fasta"):
            seqnamej=record.id
            sj=seqnamej.split('_')
            yc_j=sj[1][2]
            yc_list.append(yc_j)
            yr_j=int(sj[2].split('Yr')[1])
            yr_list.append(yr_j)
            if(len(sj)==4):
                oj=sj[3][1]
                o_list.append(oj)
            seqj=str(record.seq)
            seqj=seqj.replace("A", "0")
            seqj=seqj.replace("C", "1")
            seqj=seqj.replace("G", "2")
            seqj=seqj.replace("T", "3")
            seqj=seqj.replace("N", "4")
            mapnumj=map(int, list(seqj))
            listnumj=list(mapnumj)
            seql_list.append(listnumj)

    X_array=np.array(seql_list).astype(np.int8)
    yc_array=np.array(yc_list).astype(np.int8)
    yr_array=np.array(yr_list).astype(np.int16)
    o_array=np.array(o_list).astype(np.int8)
    return X_array, yc_array, yr_array, o_array



# Read and process fasta files to produce tensors for the deep learning model
def makeData(files):

	X_array_list=[]
	yc_array_list=[]
	yr_array_list=[]    
	o_array_list=[]
	for i in range(0, len(files)):
		X_array_i, yc_array_i, yr_array_i, o_array_i=readFastaFile(files[i])
		X_array_list.append(X_array_i)	
		yc_array_list.append(yc_array_i)
		yr_array_list.append(yr_array_i)        
		o_array_list.append(o_array_i)
		print(files[i])

	X_array=np.array(X_array_list)
	del X_array_list
	_ = gc.collect() 
	X_array=np.swapaxes(X_array,1,0)

	yc_array=np.array(yc_array_list)
	del yc_array_list
	yc_array=np.swapaxes(yc_array,1,0)
#	yc_array=np.reshape(yc_array,(yc_array.shape[0], yc_array.shape[1],1))
	yr_array=np.array(yr_array_list)
	del yr_array_list
	yr_array=np.swapaxes(yr_array,1,0)
#	yc_array=np.reshape(yc_array,(yc_array.shape[0], yc_array.shape[1],1))

	if len(files)>1:
		o_array=np.array(o_array_list[1:len(o_array_list)],dtype=o_array_list[1].dtype)
		del o_array_list
		o_array=np.swapaxes(o_array,1,0)
		o_array=np.column_stack((np.ones(o_array.shape[0]).astype(np.int8),o_array))
		
		A_array_list=[]
		for i in range(0,o_array.shape[0]):
			A_arrayi=np.outer(o_array[i,],o_array[i,])
			A_array_list.append(A_arrayi)

		A_array=np.stack(np.array(A_array_list),axis=2)
		A_array=np.swapaxes(A_array,2,0)

	else:
		A_array=[]
	
	return X_array, A_array, yc_array, yr_array



# Define function for one split
class makeAndSaveData:
    def __init__(self, assemblies, target, numSamples, binSize, context, expe, verbose=False):
        self.assemblies = assemblies
        self.target = target
        self.verbose = verbose
        self.context = context
        self.numSamples = numSamples
        self.binSize = binSize
        self.expe = expe
        
    def processSplit(self, splitk):    
    
        # File to save
        file_save='data/dataPythonGNN/test/target_'+self.target+'/bs'+str(self.binSize)+'_ct'+str(self.context)\
            +'_'+self.expe+'/split'+str(splitk)+'/test_'+str(self.numSamples)+'_bs'+str(self.binSize)\
            +'_ct'+str(self.context)+'.h5'

        if path.exists(file_save)==False:   
            # Import test data
            files_test=[]
            for i in range(0, len(self.assemblies)):
                file_test = 'data/dataPythonGNN/test/target_'+self.target+'/bs'+str(self.binSize)+'_ct'\
                    +str(self.context)+'_'+self.expe+'/split'+str(splitk)+'/test_'+str(self.numSamples)\
                    +'_bs'+str(self.binSize)+'_ct'+str(self.context)+'_'+self.assemblies[i]+'.fasta.gz'
                files_test.append(file_test)

            # Make test data
            X_test, A_test, yc_test, yr_test = makeData(files_test)
            print(X_test.shape)
            
            # Save
            hf = h5py.File(file_save, 'w')
            hf.create_dataset('X_test', data=X_test, compression="gzip")
            hf.create_dataset('A_test', data=A_test, compression="gzip")
            hf.create_dataset('yc_test', data=yc_test, compression="gzip")
            hf.create_dataset('yr_test', data=yr_test, compression="gzip")         
            hf.close()
    

# Prediction over test (by genome splits)
def makeAndSaveDataParallel(assemblies, target, numSamples, binSize, 
		context, numSplits, ncores, expe, verbose=False):
    lst = range(1,numSplits+1)
    split_lst=np.array_split(lst, np.ceil(numSplits/ncores))
    print(split_lst)
    masd=makeAndSaveData(assemblies, target, numSamples, binSize, 
    	context, expe, verbose=verbose)
    
    for k in range(0,len(split_lst)):
        jobs = []
        for splitk in split_lst[k]:
            p = multiprocessing.Process(target=masd.processSplit, args=(splitk,))
            jobs.append(p)
            p.start()
        
        for job in jobs:
            job.join()
        
