# Prediction over test (by genome splits)
import numpy as np
import h5py
from os import path
import sys
#from tensorflow.keras import backend as K
import gc
from sklearn.isotonic import IsotonicRegression
from sklearn.metrics import roc_auc_score, precision_recall_curve, average_precision_score, f1_score

sys.path.append("/media/raphael/SSD2/EvoAnnotDL/scriptPython") 
from process_data_functions import *



# Predict on test data
def predict_test(model, files_test, idxSpecies, 
	mode, batch_size, verbose=False, gnn=True):	
	y_predtest_all=np.empty((0,1))
	y_test_all=np.empty((0,1))
	
	for k in range(len(files_test)):
		file_test=files_test[k]
		if path.exists(file_test)==False:
			print("File "+file_test+" not found")
		else:
			hf = h5py.File(file_test, 'r')
			X_test = hf.get('X_test')[()]
			A_test = hf.get('A_test')[()]
			yc_test = hf.get('yc_test')[()]
			yr_test = hf.get('yr_test')[()]
			
			X_test = X_test[:,idxSpecies,]
			if len(idxSpecies)>1:
				A_test = A_test[:,idxSpecies,]
				A_test = A_test[:,:,idxSpecies]
				
			if gnn==False:
				A_test[1:,:,:]=0
						
			if mode=="regression":
				y_test=yr_test[:,0:1]	# use this to get multiple bins
			else:
				y_test=yc_test[:,0:1]
			
			# On test set (independent)
			if model.name=="CNN":
				Xall_test=X_test[:,0,:]
			elif model.name=="CNN-GNN":
				Xall_test=[X_test,A_test]
			
			# Predict	
			y_predtest=model.predict(Xall_test,batch_size=batch_size)
			
			hf.close()
			_ = gc.collect()      

			# Concatenate over splits
			if model.name=="CNN":
				y_predtest_all=np.concatenate((y_predtest_all,y_predtest))
			elif model.name=="CNN-GNN":
				y_predtest_all=np.concatenate((y_predtest_all,y_predtest))
			y_test_all=np.concatenate((y_test_all,y_test))

			# Print iterator k
			if verbose==True:
				print(k)
		    
	yobs=np.copy(y_test_all)
	ypred=np.copy(y_predtest_all)
	return (yobs,ypred)
	


# Compute metrics from model's predictions
def predictionAccuracyMetrics(yobs,ypred,mode):

	if mode=="classification":
		ypred_bin=np.copy(ypred)
		ypred_bin[ypred_bin<0.5]=0
		ypred_bin[ypred_bin>=0.5]=1
		AUROC=np.round(roc_auc_score(yobs,ypred),4)
		AUPR=np.round(average_precision_score(yobs,ypred),4)
		F1=np.round(f1_score(yobs,ypred_bin),4)
		#print(AUROC)
		#print(AUPR)
		#print(F1)
		metrics=pd.DataFrame({'AUROC':[AUROC], 'AUPR':[AUPR]})       

	elif mode=="regression":
		yobs=yobs[:,0]
		ypred=ypred[:,0]
		idxIso=range(0,int(len(yobs)*0.1))
		idxVal=range(int(len(yobs)*0.1)+1,len(yobs))
		isoRegCalibration = IsotonicRegression()
		isoRegCalibration.fit(ypred[idxIso],yobs[idxIso])
		yprediso = isoRegCalibration.predict(ypred[idxVal])
		yprediso[np.isnan(yprediso)]=0
		R=np.round(np.corrcoef(yobs[idxVal],ypred[idxVal])[0,1],4)
		Riso=np.round(np.corrcoef(yobs[idxVal],yprediso)[0,1],4)        
		#print(R)
		#print(Riso)     
		metrics=pd.DataFrame({'R':[R], 'R_isotonic':[Riso]})        
			
	return metrics

        
        
