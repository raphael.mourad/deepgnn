# Import modules
import tensorflow as tf
import numpy as np
from tensorflow.keras import backend as K
from tensorflow.keras.layers import Input, Flatten, Reshape, Dense, LSTM, Activation
from tensorflow.keras.layers import Conv1D, SeparableConv1D, GlobalMaxPooling1D, DepthwiseConv1D
from tensorflow.keras.layers import Dropout, BatchNormalization, GaussianNoise, MaxPooling1D, Masking
from tensorflow.keras.layers import Layer, Concatenate, Add, add, Bidirectional, Lambda, Attention, GRU
from tensorflow.keras.models import Model

##
# Layer for one hot encoding
def OneHot(input_dim=None, input_length=None):
	# Check if inputs were supplied correctly
	if input_dim is None or input_length is None:
		raise TypeError("input_dim or input_length is not set")

	# Helper method (not inlined for clarity)
	def _one_hot(x, num_classes):
		return K.one_hot(K.cast(x, 'uint8'), num_classes=num_classes)
		
	# Final layer representation as a Lambda layer
	return tf.keras.layers.Lambda(_one_hot, arguments={'num_classes': input_dim},input_shape=(input_length,))

##
# Functions to reshape output for CNN-GNN
def reshape_y_out_GNN(x):
    return x[:,0,]

##
# GlobalMaxPooling1D for multiple axes of None (for instance (None, None, X, X))
class GlobalMaxPooling1DGNN(Layer):
    def __init__(self):
        super(GlobalMaxPooling1DGNN, self).__init__()
        
    def call(self, inputs):
        out=tf.math.reduce_max(
            inputs, axis=(2), name="GlobalMaxPooling1DGNN"
        )
        return out

##
# Dilated convolutional layer with residual connections
class DilatedConv1D(Layer):
    def __init__(self, filters=64,
                 kernel_size=3,
                 num_dilated_convlayers=6,
                 conv_type="simple",
                 batch_norm=False,
                 activation="relu",
                 dropout=0):
        self.filters = filters
        self.kernel_size = kernel_size
        self.num_dilated_convlayers = num_dilated_convlayers
        self.conv_type = conv_type
        self.batch_norm = batch_norm
        self.activation = activation
        self.dropout = dropout


    def __call__(self, input):     
        prev_layer = input
        
        for i in range(self.num_dilated_convlayers):
            x = prev_layer
            # Type of convolution
            if self.conv_type=="simple":
            	conv_output = Conv1D(self.filters, kernel_size=self.kernel_size, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(x)           
            elif self.conv_type=="bottleneck":
            	conv_output1x1 = Conv1D(self.filters/4, kernel_size=1, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(x)            
            	conv_output3x3 = Conv1D(self.filters/4, kernel_size=self.kernel_size, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(conv_output1x1)
            	conv_output = Conv1D(self.filters, kernel_size=1, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(conv_output3x3)  
            elif self.conv_type=="inverted":
            	conv_output1x1 = Conv1D(self.filters*4, kernel_size=1, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(x)            
            	conv_output3x3 = DepthwiseConv1D(kernel_size=self.kernel_size, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(conv_output1x1)
            	conv_output = Conv1D(self.filters, kernel_size=1, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(conv_output3x3)            
            elif self.conv_type=="inverted_linear":
            	conv_output1x1 = Conv1D(self.filters*4, kernel_size=1, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(x)            
            	conv_output3x3 = DepthwiseConv1D(kernel_size=self.kernel_size, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(conv_output1x1)
            	conv_output = Conv1D(self.filters, kernel_size=1, padding='same',
                                    activation='linear', dilation_rate=2**i)(conv_output3x3)   
            elif self.conv_type=="separable":
            	conv_output = SeparableConv1D(self.filters, kernel_size=self.kernel_size, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(x)                          
            # Batch normalization
            if self.batch_norm==True:
                conv_output = BatchNormalization()(conv_output)
            
            # Dropout
            if self.dropout>0:
            	conv_output = Dropout(self.dropout)(conv_output)
            
            # Residual connection
            prev_layer = add([prev_layer, conv_output])
            combined_conv = prev_layer
        
        return combined_conv
         

##
# Graph preprocessing
class PreprocessGraph(Layer):
    def __init__(self,conv_layer,batch_size):
        super(PreprocessGraph, self).__init__()
        self.conv_layer=conv_layer
        self.batch_size=batch_size
        
    def call(self, inputs):
        if np.isin(self.conv_layer,["GCNConv","APPNPConv","DiffusionConv","GCSConv"]):
            #print(K.print_tensor(inputs))
            output_list=[]
            #for i in range(np.array(inputs).shape[0]):
            for i in range(self.batch_size):
                A=inputs[i,:,:]
                Aprime=A+tf.eye(tf.shape(A)[0])
                D=tf.linalg.diag(tf.math.reduce_sum(Aprime,axis=0))
                #print(D)
                Dprime=tf.linalg.inv(tf.sqrt(D))
                #print(Dprime)        
                xprime=tf.linalg.matmul(Dprime,Aprime)
                xprime2=tf.linalg.matmul(xprime,Dprime)
                output_list.append(xprime2)
            output=tf.stack(output_list)
            #print(K.print_tensor(output))
            #print(inputs)
            #print(output)
            #print(tf.shape(inputs))
            #print(type(output))
            #output.set_shape(shape=(None,tf.shape(inputs[0,:,:])[1],tf.shape(inputs[0,:,:])[1]))
            #output=tf.ensure_shape(output,shape=(None,23,23))
            #print(K.print_tensor(output))
            return(output)
        elif np.isin(self.conv_layer,["GATConv","AGNNConv","GraphSage","NoPrepro"]):
            output_list=[]
            for i in range(self.batch_size):
                A=inputs[i,:,:]
                output_list.append(A)
            output=tf.stack(output_list)                
            return(output)


      
##       
# My own GraphSage layer 
class GraphSage(Layer):
    def __init__(self,units=1,activation="sigmoid",aggregate='sum'):
        super(GraphSage, self).__init__()
        self.activation=activation
        self.units=units
        self.aggregate=aggregate

    def build(self, input_shape):
        self.w = self.add_weight(
            shape=(2*input_shape[0][2],self.units),
            initializer="glorot_uniform", # "glorot_uniform"
            trainable=True,
            name='kernel_weight'
        )
        self.b = self.add_weight(shape=(1,), initializer="zeros", trainable=True, name='bias_weight')

    def call(self, inputs):
        X=inputs[0] # shape = (None,6,10)
        A=inputs[1] # shape = (None,6,6)
        #print(K.print_tensor(A[0,:,:]))
        if self.aggregate=="mean":
        	A=tf.linalg.normalize(A,axis=-1,ord=1)[0] # to compute the average over nodes
        	A=tf.where(tf.math.is_nan(A), tf.zeros_like(A), A)
        AX=tf.matmul(A, X, name="matmul1_GraphSage")
        AXX=tf.concat([AX,X], axis=2)
        AXXW=tf.matmul(AXX, self.w, name="matmul2_GraphSage")+self.b
        #AXXW=tf.math.l2_normalize(AXXW,axis=-1) # Present in the original GraphSAGE but give bad results
        output=tf.keras.layers.Activation(self.activation)(AXXW)
        return output      
        


