#Create seed file with the following lines
Package: BSgenome.cavPor.UCSC.cavPor3
Title: Full genome sequences for Cavia porcellus (UCSC version 3.0)
Description: Full genome sequences for Cavia porcellus (domestic guinea pig).
Version: 3.0
organism: Cavia porcellus
common_name: Domestic guinea pig
provider: UCSC
provider_version: cavPor3
release_date: 2017-6-2
release_name: USDA/ARS (June 2017) cavPor3
source_url: http://hgdownload.soe.ucsc.edu/goldenPath/cavPor3/bigZips/
organism_biocview: Cavia_porcellus
BSgenomeObjname: cavPor3
SrcDataFiles: File: cavPor3.2bit from http://hgdownload.soe.ucsc.edu/goldenPath/cavPor3/bigZips/
seqs_srcdir: data/forge/2bit
circ_seqs: character(0)
seqfile_name: cavPor3.2bit
PkgExamples: genome$chr1 same as genome[["chr1"]]
