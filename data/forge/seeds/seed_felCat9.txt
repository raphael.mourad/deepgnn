#Create seed file with the following lines
Package: BSgenome.felCat.UCSC.felCat9
Title: Full genome sequences for Felix catus (UCSC version 9.0)
Description: Full genome sequences for Felix catus (cat).
Version: 9.0
organism: Felix catus
common_name: Cat
provider: UCSC
provider_version: felCat9
release_date: 2017-6-2
release_name: USDA/ARS (June 2017) felCat9
source_url: http://hgdownload.soe.ucsc.edu/goldenPath/felCat9/bigZips/
organism_biocview: Felix_catus
BSgenomeObjname: felCat9
SrcDataFiles: File: felCat9.2bit from http://hgdownload.soe.ucsc.edu/goldenPath/felCat9/bigZips/
seqs_srcdir: data/forge/2bit
circ_seqs: character(0)
seqfile_name: felCat9.2bit
PkgExamples: genome$chr1 same as genome[["chr1"]]
