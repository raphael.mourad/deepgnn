# DeepGNN: Semi-supervised deep learning with graph neural network for cross-species regulatory sequence prediction


![alt text](Fig1_sketch_model.png)

# Overview

We propose a novel framework for regulatory sequence prediction using semi-supervised learning with graph neural network. For this purpose, neural networks are trained not only from labeled sequences (e.g. human genome with ChIP-seq experiment), but also from unlabeled sequences (from other species without ChIP-seq experiment, e.g. chimpanzee, rabbit or dog). In order to incorporate unlabeled sequences in the training, a graph neural network connecting homologous sequences is used. Compared to supervised learning, the proposed semi-supervized learning allows to train models from a much larger number of sequences, without needing additional experiments since many unlabeled (unannotated) genomes are already available, while the vast majority of functional experiments such as ChIP-seq are only available in human, or to some extent in mouse.

# Requirements

If you have an Nvidia GPU, then you must install CUDA and cuDNN libraries. See:  
https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html  
https://developer.nvidia.com/cudnn  
Be aware that you should check the compatibility between your graphic card and the versions of CUDA and cuDNN you want to install. 
This is a bit tricky and time consuming!

To know the version of your NVIDIA driver (if you use an NVIDIA GPU) and the CUDA version, you can type:  
```
nvidia-smi
```

If you don't have an Nvidia GPU, you can use the CPU, which will be slower for deep learning computations (x20 slower).

The models were developed with python and the packages: tensorflow, keras and spektral.  

Before installing python and python packages, you need to install python3 (if you don't have it):  
```
sudo apt update
sudo apt install python3-dev python3-pip python3-venv
```

To install tensorflow:  
```
pip install --upgrade tensorflow
```

Other python packages need to be installed:   
```
pip install shuffle numpy pickle matplotlib scipy sklearn h5py tensorflow_addons spektral
```

# Basic usage

In the folder python_notebooks, you will find several python notebooks:
1) **script_preprocess_data.ipynb** (optional) to preprocess data from labeled and unlabeled DNA sequence fasta files.  
2) **script_models.ipynb** to train the semi-supervised model, implementing a convolutional network within a graph neural network (so called CNN-GNN), and also to train the classical convolutional network (CNN).  
3) **script_predict_SNP_effect.ipynb** to predict the effect of SNPs on functional data such as CTCF binding given a trained model.

# Advance Usage

If you want to use your own functional data and to train a model from these data, you need to follow the steps:
1) Install R packages using the following command:
```
install.packages(c("BSgenome","GenomicRanges","Biostrings","data.table","liftOver","pbmcapply","zoo","splicejam"))
if (!require("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
BiocManager::install("liftOver") # if liftOver installation failed with install.packages()
devtools::install_github("jmw86069/splicejam")
```

2) Some genome assemblies are already available in R as packages, so you just need to install them.  
For instance, if you want to use the mouse mm10 assembly, just install it as explained here:  
https://bioconductor.org/packages/release/data/annotation/html/BSgenome.Mmusculus.UCSC.mm10.html  
If you want to install all assemblies used in the article:
```
if (!require("BiocManager", quietly = TRUE))  
install.packages("BiocManager")  
BiocManager::install("BSgenome.Hsapiens.UCSC.hg38")  
BiocManager::install("BSgenome.Mmusculus.UCSC.mm10")
BiocManager::install("BSgenome.Btaurus.UCSC.bosTau9")
BiocManager::install("BSgenome.Cjacchus.UCSC.calJac3")
BiocManager::install("BSgenome.Cfamiliaris.UCSC.canFam3")
BiocManager::install("BSgenome.Mdomestica.UCSC.monDom5")
BiocManager::install("BSgenome.Mfuro.UCSC.musFur1")
BiocManager::install("BSgenome.Ppaniscus.UCSC.panPan2")
BiocManager::install("BSgenome.Ptroglodytes.UCSC.panTro6")
BiocManager::install("BSgenome.Mmulatta.UCSC.rheMac10")
BiocManager::install("BSgenome.Rnorvegicus.UCSC.rn7")
BiocManager::install("BSgenome.Sscrofa.UCSC.susScr11")
```

Alternatively, some assemblies are not available in R as packages, and you need to forge them.  
To forge them, you need to first download the corresponding 2bit files. You can use the shell script:
```
sh script_shell/download_2bit_assembly_files.sh
```
Them, you need to forge and install the R packages for the genome assemblies. You can use the R script (your password may be required to install the R packages):
```
Rscript script_R/script_forge_genome_assembly.R
```

3) You need to download the liftover chain files. You can use the shell script: 
```
sh script_shell/download_liftover_files.sh
```

4) Once all the forged genome assemblies and the liftover chain files are available, you can generate the labeled DNA sequences with the homologous unlabeled DNA sequences and save them as fastq files. You can use the R script "script_R/script_generate_data.R" for that.  
You must choose whether you want to generate data for training a model (mode="train" in the R script) or for testing the prediction accuracy of the model (mode="test"). It is advised to carefully check the "Data generation parameters" in the R script.

5) Now you can use your data to train a model and predict. Go to "Basic usage" section above.  

# Contact: 
raphael.mourad@univ-tlse3.fr
